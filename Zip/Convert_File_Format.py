# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 11:29:37 2021

@author: Stoyan Boyukliyski
"""

import json
import csv
from datetime import datetime
from tzlocal import get_localzone
import pandas as pd
import os

class Reader():
    def __init__(self, input_file = None):
        if input_file == None:
            self.input_file_path = self.input_directory()
        else:
            self.input_file_path = input_file
        self.local_timezone = get_localzone()
        
        if self.input_file_path.split('.')[-1] == 'json':
            self.convert_from_json()
        elif self.input_file_path.split('.')[-1] == 'csv':
            self.convert_from_csv()
    
    def input_directory(self):
        input_directory = input('Please, specify a file path to an input csv or json file: ')
        if os.path.exists(input_directory):
            return input_directory
        else:
            if (input_directory.split('.')[-1] == 'json' or input_directory.split('.')[-1] == 'csv'):
                return input_directory
            else:
                input_directory = self.input_directory()
            
    def convert_from_json(self):
        myjsonfile = open(self.input_file_path)
        dataset = json.loads(myjsonfile.read())
        
        self.new_dataset = [{'city' : objects['city'],
                             'date' : self.datetime_conversion(objects['date']),
                             'temp' : self.time_conversion(objects['temp'])} for objects in dataset]
        
    def convert_from_csv(self):
        dataset= pd.read_csv(self.input_file_path, header = None)
        
        self.new_dataset = [{'city' : rows[0], 
                             'date' : self.datetime_conversion(rows[1]),
                             'temp' : self.time_conversion(rows[2])} for index, rows in dataset.iterrows()]
        
    def datetime_conversion(self, date):
        return str(datetime.strptime(date.split()[-1],'%Y-%m-%dT%H:%M:%S%z').astimezone(self.local_timezone))
    
    def datetime_conversion_2(self, date):
        return str(datetime.strptime(date,' %Y-%m-%dT%H:%M:%S%z').astimezone(self.local_timezone))
    
    def time_conversion(self, date):
        return str(int(int(date.split('C')[0])*9/5 + 32)) + 'F'
    
    
        
class Writer():
    def __init__(self, dataset, output_file = None):
        if output_file == None:
            self.file_name = self.output_directory()
        else:
            self.file_name = output_file
        self.dataset = dataset
        
        if self.file_name.split('.')[-1] == 'json':
            self._writer_json()
        elif self.file_name.split('.')[-1] == 'csv':
            self._writer_csv()
        else:
            raise("The specified file format isn't valid")
        
    def output_directory(self):
        output_directory = input('Please, specify a file path to an output csv or json file: ') 
        if (output_directory.split('.')[-1] == 'json' or output_directory.split('.')[-1] == 'csv'):
            return output_directory
        else:
            output_directory = self.output_directory()
            
    def _writer_json(self):
        with open(self.file_name, 'w', encoding='utf-8') as f:
            json.dump(self.dataset, f, ensure_ascii=False, indent=4)
            
    def _writer_csv(self):
        with open(self.file_name, 'w', encoding='UTF8', newline='') as f:
            writer = csv.DictWriter(f, fieldnames = ['city', 'date', 'temp'])
            writer.writerows(self.dataset)