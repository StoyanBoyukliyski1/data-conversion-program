# -*- coding: utf-8 -*-
"""
Created on Sun Oct 31 11:22:56 2021

@author: Stoyan Boyukliyski
"""

import unittest
import Convert_File_Format as cff
import pandas as pd

class ConvertTester(unittest.TestCase):
        
    def test_time_conversion(self):
        reader = cff.Reader(input_file = "Input_File.json")
        
        self.assertEqual(reader.datetime_conversion("2021-08-01T13:00:00+02:00") ,'2021-08-01 14:00:00+03:00')
        self.assertEqual(reader.datetime_conversion("2021-08-01T11:45:00Z") ,'2021-08-01 14:45:00+03:00')
        
        self.assertEqual(reader.datetime_conversion(" 2021-08-01T13:00:00+02:00") ,'2021-08-01 14:00:00+03:00')
        self.assertEqual(reader.datetime_conversion(" 2021-08-01T11:45:00Z") ,'2021-08-01 14:45:00+03:00')
        
        
    def test_temperature_conversion(self):
        reader = cff.Reader(input_file = "Input_File.json")
        
        self.assertEqual(reader.time_conversion("33C") ,'91F')
        self.assertEqual(reader.time_conversion("25 C") ,'77F')
        
if __name__ == '__main__':
    unittest.main()
    