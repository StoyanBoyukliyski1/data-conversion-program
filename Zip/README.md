To run the code, please open main_file.py

Once opened the programs prompts the user for an input file. Depending on the ending .csv or .json file the program will direct the logic towards a csv or json reader.
If the file is non-existant you will be asked to enter the file again.

After that the program will ask the user for an output file. The output file should have an extension of .csv and .json. A new file will be created automatically or an 
old file would be overwritten. 

The program is able to: 
read .json write .json
read .csv write .json
read .json write .csv
read .csv write .csv